from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response


def hello_world(request):
    config = Configurator(request.registry)
    config.add_view(
        config.get_settings().get('greetings', hello_world).__next__(),
        route_name='hello')
    config.commit()
    return Response('<body><h1>Hello World!</h1></body>')

def hi_world(request):
    config = Configurator(request.registry)
    config.add_view(
        config.get_settings().get('greetings', hello_world).__next__(),
        route_name='hello')
    config.commit()
    return Response('<body><h1>Hi World!</h1></body>')


def swich_greetings(view_functions):
    n = 0
    while n < len(view_functions):
        yield view_functions[n]
        n += 1
        if n == len(view_functions):
            n = 0


def main():
    config = Configurator()
    greetings = swich_greetings([hello_world, hi_world])
    config.add_settings(greetings=greetings)
    config.add_route('hello', '/')
    config.add_view(greetings.__next__(), route_name='hello')
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()

if __name__ == '__main__':
    main()